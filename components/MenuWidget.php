<?php
/**
 * Created by PhpStorm.
 * User: kas
 * Date: 10.03.2017
 * Time: 13:23
 */

namespace app\components;

use yii\base\Widget;
use app\models\Category;
class MenuWidget extends Widget
{
    public $data;
    public $tree;
    public $menuHtml;
    public $tpl;

    public function init()
    {
        parent::init();
        if($this->tpl === null){
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';


}

    public function run()
    {
        $this->data = Category::find()->indexBy('id')->asArray()->all();
        $this->tree = $this->getTree();
        debug($this->data);
        return $this->menuHtml;
    }

    protected function getTree(){
        $tree = [];
        foreach ($this->data as $id=>&$node) {
            if (!$node['parent_id'])
                $tree[$id] = &$node;
            else
                $this->data[$node['parent_id']]['childs'][$node['id']] = &$node;
        }
        return $tree;
    }

}