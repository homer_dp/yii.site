<?php
/**
 * Created by PhpStorm.
 * User: kas
 * Date: 10.03.2017
 * Time: 13:15
 */

namespace app\models;


use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'product';
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::className(),['id' => 'category_id'] );
    }
    
}