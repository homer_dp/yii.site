<?php
/**
 * Created by PhpStorm.
 * User: kas
 * Date: 10.03.2017
 * Time: 13:11
 */

namespace app\models;


use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public static function tableName()
    {
        return 'category';
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(),['category_id' => 'id'] );
    }
}